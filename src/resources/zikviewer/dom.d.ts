interface Document {
  fullscreenElement: HTMLElement;
  pointerLockElement: Element;
  exitPointerLock(): void;
}

interface HTMLElement {
  requestPointerLock(): void;
}

interface Event {
  wheelDelta: number;
}
