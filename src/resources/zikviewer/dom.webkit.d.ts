interface Document {
  webkitFullscreenElement: HTMLElement;
  webkitExitFullscreen(): void;
}

interface HTMLElement {
  webkitRequestFullscreen(): void;
}
