import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Replay } from '../shared/models/replay.model';
import { ReplayService } from '../shared/services/replay.service';

@Component({
  selector: 'app-replay-search',
  templateUrl: './replay-search.component.html',
  styleUrls: ['./replay-search.component.scss']
})
export class ReplaySearchComponent implements OnInit {
  private replays: Observable<Replay[]>;

  constructor(private replayService: ReplayService) { }

  ngOnInit() {
    this.getReplays();
  }

  getReplays(): void {
    this.replays = this.replayService.getReplays();
  }

}
