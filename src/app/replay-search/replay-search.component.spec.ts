import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReplaySearchComponent } from './replay-search.component';

describe('ReplaySearchComponent', () => {
  let component: ReplaySearchComponent;
  let fixture: ComponentFixture<ReplaySearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReplaySearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReplaySearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
