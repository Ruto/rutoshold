import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  mapBaseUrl = 'https://devruto.com';
  apiUrl = 'https://ruto.sh/api';

  constructor() {}

}
