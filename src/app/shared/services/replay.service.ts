import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable, of } from 'rxjs';
import {catchError} from 'rxjs/operators';
import { Replay } from '../models/replay.model';
import { ConfigService } from './config.service';

@Injectable({
  providedIn: 'root'
})
export class ReplayService {
  private replaysUrl = this.config.apiUrl + '/replays';

  constructor(
    private http: HttpClient,
    private config: ConfigService) {
}

  getReplays(): Observable<Replay[]> {
    return this.http.get<Replay[]>(this.replaysUrl).pipe(catchError(this.handleError('getReplays', [])));
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    }
  }
}
