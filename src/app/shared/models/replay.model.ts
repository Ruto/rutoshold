export interface Replay {
    id: number;
    steam_id32: number;
    alias: string;
    mode: number;
    map: string;
    stage: number;
    run_time: number;
    teleports: number;
    created: string;
}