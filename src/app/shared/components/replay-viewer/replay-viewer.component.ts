import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, Input } from '@angular/core';
import { ReplayViewer } from 'src/resources/zikviewer/ReplayViewer';
import { CameraMode } from 'src/resources/zikviewer/SourceUtils/MapViewer';
import { ActivatedRoute } from '@angular/router';
import { ConfigService } from '../../services/config.service';

@Component({
  selector: 'app-replay-viewer',
  templateUrl: './replay-viewer.component.html',
  styleUrls: ['./replay-viewer.component.scss']
})
export class ReplayViewerComponent implements AfterViewInit, OnInit {

  @Input()
  id: number;

  @ViewChild('mapview')
  mapView: ElementRef;

  viewer: ReplayViewer;

  constructor(private config: ConfigService) { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.viewer = new ReplayViewer(this.mapView.nativeElement);
    this.viewer.cameraMode = CameraMode.Fixed;
    this.viewer.mapBaseUrl = this.config.mapBaseUrl;

    this.viewer.isPlaying = true;
    this.viewer.showDebugPanel = true;
    let replayUrl = 'https://devruto.com/api/replays/' + this.id;
    this.viewer.loadReplay(replayUrl);
    this.viewer.animate();
  }

}
