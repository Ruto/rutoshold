import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { ReplayViewer } from 'src/resources/zikviewer/ReplayViewer';
import { ConfigService } from '../../services/config.service';

@Component({
  selector: 'app-map-viewer',
  templateUrl: './map-viewer.component.html',
  styleUrls: ['./map-viewer.component.scss']
})
export class MapViewerComponent implements OnInit {

  @Input()
  mapName: string;

  @ViewChild('mapview')
  mapView: ElementRef;

  viewer: ReplayViewer;

  constructor(private config: ConfigService) { }

  ngOnInit() {
    this.viewer = new ReplayViewer(this.mapView.nativeElement);
    const version = new Date().getTime().toString(16);
    this.viewer.loadMap(`/resources/csgo/maps/${this.mapName}/index.json?v=${version}`, this.config.mapBaseUrl);
    this.viewer.animate();
  }

}
