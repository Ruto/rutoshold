import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  isMobile: boolean;

  constructor() { }

  ngOnInit() {
  }

  toggleNav() {
    this.isMobile = !this.isMobile;
  }

}
