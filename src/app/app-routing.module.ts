import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReplayComponent } from './replay/replay.component';
import { MapComponent } from './map/map.component';
import { ReplaySearchComponent } from './replay-search/replay-search.component';

const routes: Routes = [
  {
    path: 'replays',
    component: ReplaySearchComponent
  },
  {
    path: 'replay/:id',
    component: ReplayComponent
  },
  {
    path: 'map/:name',
    component: MapComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
