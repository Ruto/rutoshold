import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

  mapName: string;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.mapName = this.route.snapshot.paramMap.get('name');
  }

}
