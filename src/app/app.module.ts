import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BlogComponent } from './blog/blog.component';
import { ReplayService } from './shared/services/replay.service';
import { ReplayViewerComponent } from './shared/components/replay-viewer/replay-viewer.component';
import { MapViewerComponent } from './shared/components/map-viewer/map-viewer.component';
import { ReplayComponent } from './replay/replay.component';
import { ReplaySearchComponent } from './replay-search/replay-search.component';
import { MapComponent } from './map/map.component';
import { ConfigService } from './shared/services/config.service';
import { NavbarComponent } from './shared/components/navbar/navbar.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    BlogComponent,
    ReplayViewerComponent,
    MapViewerComponent,
    ReplayComponent,
    ReplaySearchComponent,
    MapComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    ReplayService,
    ConfigService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
